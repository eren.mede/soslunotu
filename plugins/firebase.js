import firebase from 'firebase/app'
import 'firebase/firestore'

const config = {
    apiKey: "AIzaSyD3fUr5jx_x64LfuwOY5XZQodHBCApg4io",
    authDomain: "sozlunotu.firebaseapp.com",
    databaseURL: "https://sozlunotu.firebaseio.com",
    projectId: "sozlunotu",
    storageBucket: "sozlunotu.appspot.com",
    messagingSenderId: "956818848049",
    appId: "1:956818848049:web:a505eebf20f729e2309810",
    measurementId: "G-MB6P1YZQ5J"
}

if (!firebase.apps.length) {
  firebase.initializeApp(config)
}


export const fb = firebase.firestore()

export default firebase